package com.tsystems.javaschool.tasks.subsequence;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param args
     */
    @SuppressWarnings("rawtypes")
    public static void main(String args[]) {
        Subsequence s = new Subsequence();
        boolean b = s.find(Arrays.asList("A", "B", "C", "D"),
        Arrays.asList("BD", "A", "ABC", "B", "M", "D", "M", "C", "DC", "D"));
        System.out.println(b);
    }
    public boolean find(List x, List y) {
        Stack <String> stack_x = new Stack();
        Stack <String> stack_y = new Stack();
        String[] array_x = (String[]) x.toArray(new String[0]);
        String[] array_y = (String[]) y.toArray(new String[0]);
        for (String array_x1 : array_x) {
            stack_x.push(array_x1);
        }
        for (String array_y1 : array_y) {
            stack_y.push(array_y1);
        }
        while (!stack_y.isEmpty()) {
            if (!stack_x.isEmpty()) {
                if (stack_x.peek().equals(stack_y.peek())) {
                    stack_x.pop();
                    stack_y.pop();
                } else stack_y.pop();
            } else break;
        }
        return stack_x.isEmpty();
    }
}
