package com.tsystems.javaschool.tasks.pyramid;
    public class CannotBuildPyramidException extends RuntimeException {
        CannotBuildPyramidException (){
        }
    /*The exception is prescribed based on the fact that when searching for the number of rows, 
    the root of the quadratic equation is found, and if the number is not an integer, 
    then the pyramid cannot be built*/
    public void CannotBuild(double radix) throws CannotBuildPyramidException {
        String str = Double.toString(radix);
        String substr = str.substring(str.indexOf('.'));
        if (substr.length()>2) {
            System.out.println ("Pyramide cannot be build!");
            System.exit(0);
        }
    }
}
