package com.tsystems.javaschool.tasks.pyramid;

import static java.lang.Math.sqrt;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right).All vacant positions in the array are zeros.
     *
     */
    public static void main(String args[]) {
        int [] input = {12, 11, 16, 12, 3, 5};
        List<Integer> inputNumbers = new ArrayList<>(input.length);
        for (int i : input) {
            inputNumbers.add(i);
        }
        PyramidBuilder pyramid = new PyramidBuilder();
        pyramid.buildPyramid(inputNumbers);
    }
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int row; //number of rows of pyramid i
        int elements = inputNumbers.size(); //number of elements in the original array
        int length; //length of one pyramid row j
        int position = 0; //starting position of the number in the pyramid
        int elements_in_row;
        double radix = 0;
        CannotBuildPyramidException pyr = new CannotBuildPyramidException();
        try {
            radix = -0.5+sqrt(0.25+4*0.5*elements); /*solution of a quadratic equation: 0.5x ^ 2 + 0.5x-elements = 0 
            (the discriminant is always positive, the coefficients are taken from the arithmetic 
            progression, only plus is taken, since the number of rows will not be negative*/
            pyr.CannotBuild(radix);
        } catch (CannotBuildPyramidException ex) {           
        }
        row = (int) (radix); 
        length = 1+(row-1)*2; /*according to the formula of arithmetic progression, the length of the 
        bottom row (as well as the length of the array) increases by 2 with each increase in the row by 1*/
        ArrayList<ArrayList<Integer>> table = new ArrayList<>(row);
        ArrayList <Integer> nums = new ArrayList<>();
        int order = 0; //sequence of elements in the array nums
        Integer[] array = (Integer[])inputNumbers.toArray(new Integer[inputNumbers.size()]); //source array
        Arrays.sort(array);
        nums.addAll(Arrays.asList(array));
        for(int i=0; i < row; i++) {
            table.add(new ArrayList());
        }
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < length; j++) {
                table.get(i).add(0);
            }
        }
        int rest_elements=0;
        for (int i = 0; i < row; i++ ) {
            elements_in_row = (i+1);
            rest_elements=0;
            position = length/2 - elements_in_row + 1;
            for (int j = position; j < length;) {
                while (rest_elements!=elements_in_row){
                    table.get(i).set(j, nums.get(order));
                    order++;
                    rest_elements = rest_elements + 1;  
                    j+=2;
                }
                if (rest_elements==elements_in_row) {
                    j = length;
                }
            }
        }
        int[][] arr = new int[table.size()][];
	for (int i = 0; i < arr.length; i++) {
            arr[i] = new int[table.get(i).size()];
	}
	for(int i=0; i<table.size(); i++){
            for (int j = 0; j < table.get(i).size(); j++) {
		arr[i][j] = table.get(i).get(j);
                System.out.print(arr[i][j]);
            }
            System.out.println();
	}
        return arr;
    }
}
